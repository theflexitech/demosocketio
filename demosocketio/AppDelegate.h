//
//  AppDelegate.h
//  demosocketio
//
//  Created by Promsopheak on 5/10/16.
//  Copyright © 2016 Promsopheak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

