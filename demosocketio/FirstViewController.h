//
//  FirstViewController.h
//  demosocketio
//
//  Created by Promsopheak on 5/10/16.
//  Copyright © 2016 Promsopheak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Vendors/socket.IO-objc/SocketIO.h"

@interface FirstViewController : UIViewController <SocketIODelegate>
{
    SocketIO *socketIO;
}
@end

