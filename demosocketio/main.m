//
//  main.m
//  demosocketio
//
//  Created by Promsopheak on 5/10/16.
//  Copyright © 2016 Promsopheak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
