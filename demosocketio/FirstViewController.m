//
//  FirstViewController.m
//  demosocketio
//
//  Created by Promsopheak on 5/10/16.
//  Copyright © 2016 Promsopheak. All rights reserved.
//

#import "FirstViewController.h"
#import "Vendors/socket.IO-objc/SocketIO.h"
#import "Vendors/socket.IO-objc/SocketIOPacket.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Socket Connection
    socketIO = [[SocketIO alloc] initWithDelegate:self];
    [socketIO connectToHost:@"192.168.0.109" onPort:3000 withParams:nil withNamespace:@"/users"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


# pragma mark -
# pragma mark socket.IO-objc delegate methods

- (void) socketIODidConnect:(SocketIO *)socket
{
    NSLog(@"socket.io connected.");
}

- (void) socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet
{
    NSLog(@"didReceiveEvent()");
    if ([packet.name isEqualToString:@"authorization"] ){
        // test different event data types
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setObject:@"testing" forKey:@"sessionId"];
        [dict setObject:@"HMykRXgqd6mYN7Z2a2/nP1CvNUaFVr9ENYjHyoDP83E=" forKey:@"token"];
    
        SocketIOCallback cb = ^(id argsData) {
            NSDictionary *response = argsData;
            // do something with response
            NSLog(@"didReceiveEvent() %@", response);
        };
        [socketIO sendEvent:@"authorization" withData:dict andAcknowledge:cb];
    }
}

- (void) socketIO:(SocketIO *)socket onError:(NSError *)error
{
    if ([error code] == SocketIOUnauthorized) {
        NSLog(@"not authorized");
    } else {
        NSLog(@"onError() %@", error);
    }
}


- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error
{
    NSLog(@"socket.io disconnected. did error occur? %@", error);
}


@end
